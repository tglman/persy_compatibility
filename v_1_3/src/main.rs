
use persy::{ByteVec, Persy, PersyId};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let persy = Persy::open("../data_1_2.db", persy::Config::new())?;
    let mut id = persy.get::<String, PersyId>("index_gen", &"key".to_string())?;
    let record = persy.read("data", &id.next().unwrap()) ?;
    assert_eq!(record.unwrap(), "aaaa".as_bytes());
    assert_eq!(persy.get::<u8, u8>("index_u8", &10)?.next(), Some(10));
    assert_eq!(persy.get::<u16, u16>("index_u16", &10)?.next(), Some(10));
    assert_eq!(persy.get::<u32, u32>("index_u32", &10)?.next(), Some(10));
    assert_eq!(persy.get::<u64, u64>("index_u64", &10)?.next(), Some(10));
    assert_eq!(persy.get::<u128, u128>("index_u128", &10)?.next(), Some(10));
    assert_eq!(persy.get::<i8, i8>("index_i8", &10)?.next(), Some(10));
    assert_eq!(persy.get::<i32, i32>("index_i32", &10)?.next(), Some(10));
    assert_eq!(persy.get::<i16, i16>("index_i16", &10)?.next(), Some(10));
    assert_eq!(persy.get::<i64, i64>("index_i64", &10)?.next(), Some(10));
    assert_eq!(persy.get::<i128, i128>("index_i128", &10)?.next(), Some(10));
    assert_eq!(persy.get::<f32, f32>("index_f32", &10.0)?.next(), Some(10.0));
    assert_eq!(persy.get::<f64, f64>("index_f64", &10.0)?.next(), Some(10.0));
    assert_eq!(persy.get::<String, String>("index_string", &String::from("aaa"))?.next(),Some( String::from("aaa")));
    assert_eq!(persy.get::<ByteVec, ByteVec>("index_bytes", &ByteVec::from("aaa".as_bytes()),)?.next(),
        Some(ByteVec::from("aaa".as_bytes())),
    );
    Ok(())
}
