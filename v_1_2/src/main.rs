use persy::{ByteVec, Persy, PersyId, ValueMode};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    Persy::create("../data_1_2.db")?;

    let persy = Persy::open("../data_1_2.db", persy::Config::new())?;
    let mut tx = persy.begin()?;
    tx.create_segment("data")?;
    tx.create_index::<String, PersyId>("index_gen", ValueMode::Replace)?;
    tx.create_index::<u8, u8>("index_u8", ValueMode::Replace)?;
    tx.create_index::<u16, u16>("index_u16", ValueMode::Replace)?;
    tx.create_index::<u32, u32>("index_u32", ValueMode::Replace)?;
    tx.create_index::<u64, u64>("index_u64", ValueMode::Replace)?;
    tx.create_index::<u128, u128>("index_u128", ValueMode::Replace)?;
    tx.create_index::<i8, i8>("index_i8", ValueMode::Replace)?;
    tx.create_index::<i16, i16>("index_i16", ValueMode::Replace)?;
    tx.create_index::<i32, i32>("index_i32", ValueMode::Replace)?;
    tx.create_index::<i64, i64>("index_i64", ValueMode::Replace)?;
    tx.create_index::<i128, i128>("index_i128", ValueMode::Replace)?;
    tx.create_index::<f32, f32>("index_f32", ValueMode::Replace)?;
    tx.create_index::<f64, f64>("index_f64", ValueMode::Replace)?;
    tx.create_index::<String, String>("index_string", ValueMode::Replace)?;
    tx.create_index::<ByteVec, ByteVec>("index_bytes", ValueMode::Replace)?;
    let prepared = tx.prepare()?;
    prepared.commit()?;
    let mut tx = persy.begin()?;
    let rec = "aaaa".as_bytes();
    let id = tx.insert("data", rec)?;

    tx.put::<String, PersyId>("index_gen", "key".to_string(), id)?;
    tx.put::<u8, u8>("index_u8", 10, 10)?;
    tx.put::<u16, u16>("index_u16", 10, 10)?;
    tx.put::<u32, u32>("index_u32", 10, 10)?;
    tx.put::<u64, u64>("index_u64", 10, 10)?;
    tx.put::<u128, u128>("index_u128", 10, 10)?;
    tx.put::<i8, i8>("index_i8", 10, 10)?;
    tx.put::<i16, i16>("index_i16", 10, 10)?;
    tx.put::<i32, i32>("index_i32", 10, 10)?;
    tx.put::<i64, i64>("index_i64", 10, 10)?;
    tx.put::<i128, i128>("index_i128", 10, 10)?;
    tx.put::<f32, f32>("index_f32", 10.0, 10.0)?;
    tx.put::<f64, f64>("index_f64", 10.0, 10.0)?;
    tx.put::<String, String>("index_string", String::from("aaa"), String::from("aaa"))?;
    tx.put::<ByteVec, ByteVec>(
        "index_bytes",
        ByteVec::from("aaa".as_bytes()),
        ByteVec::from("aaa".as_bytes()),
    )?;

    let prepared = tx.prepare()?;
    prepared.commit()?;

    Ok(())
}
