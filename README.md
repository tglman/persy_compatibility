## Persy disk compatibilty project

Simple set of rust projects that use different version of persy, with the aim to test compatibility and migration of disk
structures.

To check the status run the script "build_and_check.sh" and see if terminates correctly.


This project will not include all the Persy versions but only the one where disk structure changes have been done.



